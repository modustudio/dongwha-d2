//<![CDATA[
/*  left menu */
/* popupzone start */
var maxSize = 4; 		// 최대 보여질 알림존 수;
var totalSize = 0; // 보여줄 현재 알림존 수
var time_delay = 3000; 	// 3초 딜레이

var popupzoneWhich = 2; 	// 알림존 시작 위치 
var popupzoneTimer;
//var image_btn_path = "/main/images/main/";
var image_btn_path = "/images/BrandMac/";
$(document).ready(function () {
	totalSize = $(".popzone_img").length;
	menuShow();
	$(".dev_cate1Menu").click(function () {
		var showIndex = $(this).parent("li").index();
		if ($(".dev_subMenu:eq(" + showIndex + ")").css("display") == "none") {
			$(".dev_subMenu").hide();
			$(".dev_subMenu:eq(" + showIndex + ")").show();
		} else {
			$(".dev_subMenu:eq(" + showIndex + ")").hide();
		}
	});
		
	tooltip();
	if ($(".contents_home").length != 0) { StartPopupzone(); }

	/*selectbox*/
	$('.formSelect').sSelect();

	/* 컬러 tab
	$("#primary a").click(function(){
	$("#primary a").parent('li').removeClass("on");
	$(this).parent('li').addClass("on");
	$("#Content div").css("display", "none");
	var tabname = $(this).parent('li').index();
	tabname = 'tabMenu'+tabname ;
	$("#Content #"+tabname).css("display","block");

	}); */

});

function menuShow() {
	var nowAllUrl = location.pathname + location.search;
	var nowUrl = location.pathname;
	
	if (nowUrl.indexOf("newCollection.aspx") >= 0 || nowUrl.indexOf("collectionProductList.aspx") >= 0) {
		$(".dev_subMenu:eq(0)").show();
		$(".dev_cate1Menu:eq(0)").css("font-weight", "bold");
		$(".dev_cate1Menu:eq(0)").css("background", "url(/images/BrandMac/dot_on.gif) no-repeat 139px 50%");
	} else {
		var nowUrlParam = location.search;
		if (nowUrlParam.indexOf("bestNum=") >= 0) {
			$(".dev_subMenu:eq(1)").show();
			$(".dev_cate1Menu:eq(1)").css("font-weight", "bold");
			$(".dev_cate1Menu:eq(1)").css("background", "url(/images/BrandMac/dot_on.gif) no-repeat 139px 50%");
		} else {
			if (nowUrlParam != "") {
				var nowUrlParamArr = nowUrlParam.split('=');
				var nowUrlParamArr2 = nowUrlParamArr[1].split('&');
				var showIndex = parseInt(nowUrlParamArr2[0], 10) + 1;
				$(".dev_subMenu:eq(" + showIndex + ")").show();
				$(".dev_cate1Menu:eq(" + showIndex + ")").css("font-weight", "bold");
				$(".dev_cate1Menu:eq(" + showIndex + ")").css("background", "url(/images/BrandMac/dot_on.gif) no-repeat 139px 50%");
			}
		}
	}

	if ($('.dev_subMenu a[href="' + nowAllUrl + '"]') != null) {
		$('.dev_subMenu a[href="' + nowAllUrl + '"]').css("font-weight", "bold");
	}
}

function forwardPopupzone(){
	///////////////////////////
	var total_page = parseInt((popupzoneWhich-1) / maxSize)+1;

	var start_btn_no = parseInt((total_page-1)*maxSize+1);
	
	if(start_btn_no>totalSize){
		start_btn_no =  start_btn_no - maxSize
	}
	var end_btn_no = start_btn_no + maxSize - 1;
	if(end_btn_no>totalSize){
		end_btn_no = totalSize;
	}
	///////////////////////////

	displayPoupzone(popupzoneWhich, start_btn_no, end_btn_no);
	
	popupzoneWhich++;
	if (popupzoneWhich > totalSize) popupzoneWhich = 1;

	
}
function forwardPopupzoneDirect(no){
	StopPopupzone();
	for (var i=1;i<=totalSize;i++) {
		var fldbtn = document.getElementById("popupzoneNumber"+i);
		fldbtn.src = (i == no) ? image_btn_path + '0'+i+'_over.gif' : image_btn_path + '0'+i+'_out.gif';
		var fld = document.getElementById("popupzone_content_"+i);
		fld.style.visibility = (i == no) ? 'visible' : 'hidden';
	}

}

function displayPoupzone(select_no, start_btn_no, end_btn_no){
	for (var i=1;i<=totalSize;i++) {
		var fldbtn = document.getElementById("popupzoneNumber"+i);
		fldbtn.src = (i == popupzoneWhich) ? image_btn_path + '0'+i+'_over.gif' : image_btn_path + '0'+i+'_out.gif';
		var fld = document.getElementById("popupzone_content_"+i);
		fld.style.visibility = (i == select_no) ? 'visible' : 'hidden';


		//popupzone_btn_1~12
		var popzone_area = document.getElementById("popzone_"+i);
	
		if(start_btn_no<=i && i<=end_btn_no){
			popzone_area.style.visibility = 'visible';
		}else{
			popzone_area.style.visibility = 'hidden';
		}
		//////////////////////////

	}
}

function prevPopupzone(){
	popupzoneWhich -= maxSize;
	
	popupzoneWhich1 = popupzoneWhich;
	popupzoneWhich = parseInt(popupzoneWhich / maxSize) * maxSize + 1;

	if (popupzoneWhich1 < 1) {
		popupzoneWhich = totalSize;
	}
	///////////////////////////
	var total_page = parseInt((popupzoneWhich-1) / maxSize)+1;
	var start_btn_no = parseInt((total_page-1)*maxSize+1);
	var end_btn_no = start_btn_no + maxSize - 1;
	if(end_btn_no>totalSize){
		end_btn_no = totalSize;
	}
	//////////////////////////
	displayPoupzone(popupzoneWhich, start_btn_no, end_btn_no);
	StartPopupzone();
}
function nextPopupzone(){
	popupzoneWhich += maxSize;
	/*
	if(popupzoneWhich > totalSize){
		popupzoneWhich = totalSize;
	}
	*/

	popupzoneWhich1 = popupzoneWhich;
	popupzoneWhich = parseInt(popupzoneWhich / maxSize) * maxSize + 1;

	if (popupzoneWhich1 > totalSize) {
		popupzoneWhich = 1;
	}
	///////////////////////////
	var total_page = parseInt((popupzoneWhich-1) / maxSize)+1;
	var start_btn_no = parseInt((total_page-1)*maxSize+1);
	if(start_btn_no>totalSize){
		start_btn_no =  start_btn_no - maxSize
	}
	var end_btn_no = start_btn_no + maxSize - 1;
	if(end_btn_no>totalSize){
		end_btn_no = totalSize;
	}
	
	displayPoupzone(popupzoneWhich, start_btn_no, end_btn_no);
	StartPopupzone();

}
function StopPopupzone() {
	clearInterval(popupzoneTimer);
}
function StartPopupzone() {
	clearInterval(popupzoneTimer);
	popupzoneTimer = setInterval('forwardPopupzone()',time_delay);
}

this.tooltip = function () {
	/* CONFIG */
	xOffset = -5;
	yOffset = 15;
	// these 2 variable determine popup's distance from the cursor
	// you might want to adjust to get the right result		
	/* END CONFIG */
	var colortitle = '';

	

	$("a.colortooltip").click(function (e) {
		$(this).parents('.content_list').find(".buy_box #colorName").text(colortitle);
	});


	$("a.detailtooltip").hover(function (e) {
		$(this).find('span').css('display', 'block');
	},
	function () {
		$(this).find('span').css('display', 'none');
	});
};



// 답변보기 
jQuery(function(){
	
	var article = $('#tabMenu2 .article td');

	article.addClass('hide');
	article.slideUp(100);

	var selectText  = $('#tabMenu2 .trigger');

	$('#tabMenu2 .trigger').click(function(){
		var myArticle = $(this).parents('tr').next().find('td');
	
		if(myArticle.hasClass('hide')){
			
			selectText.removeClass('selectList');
			article.addClass('hide').removeClass('show');
			article.slideUp(100);
			myArticle.removeClass('hide').addClass('show');
			myArticle.slideDown(100);
			$(this).addClass('selectList');
		} else {
			myArticle.removeClass('show').addClass('hide');
			myArticle.slideUp(100);
			$(this).removeClass('selectList');
		}
	});
	
});

// 답변보기 
jQuery(function(){
	
	var article = $('.tabMenucall .article td');

	article.addClass('hide');
	article.slideUp(100);

	var selectText  = $('.tabMenucall .trigger');

	$('.tabMenucall .trigger').click(function(){
		var myArticle = $(this).parents('tr').next().find('td');
	
		if(myArticle.hasClass('hide')){
			
			selectText.removeClass('selectList');
			article.addClass('hide').removeClass('show');
			article.slideUp(100);
			myArticle.removeClass('hide').addClass('show');
			myArticle.slideDown(100);
			$(this).addClass('selectList');
		} else {
			myArticle.removeClass('show').addClass('hide');
			myArticle.slideUp(100);
			$(this).removeClass('selectList');
		}
	});
	
});


//]]>

//상품인도방법
function viewTabPro(tot, n){
	for(i=1; i<= tot; i++){
	    if (i == n) {
	        document.getElementById('tabPro' + i).src = "http://image.dutyfree24.com/FileStroage/BRAND/MAC/btn_air" + i + "_on.gif"
			document.getElementById('dutyfree'+i).style.display = "block"
		}
		else{
		    document.getElementById('tabPro' + i).src = "http://image.dutyfree24.com/FileStroage/BRAND/MAC/btn_air" + i + ".gif"
			document.getElementById('dutyfree'+i).style.display = "none"
		}
	}
}

function macAjax(param) {
    $.ajax({
        url: "/brandmall/mac/mac_proc.aspx",
        type: "POST",
        data: param,
        error: function (info, xhr) {
            alert(info.responseText);
        },
        success: function (req) {
            var r = eval('(' + req + ')');

            if (r.status == "F") {
                alert(r.msg);
            }
            else if (r.status == "L") {
                if (r.msg != "") {
                    alert(r.msg);
                }
                location.href = r.url;
            }
            else if (r.status == "C") {
                if (r.msg != "") {
                    if (confirm(r.msg)) {
                        location.href = r.url;
                    }
                }
            }
            else if (r.status == "S") {
                if (r.msg != "") {
                    alert(r.msg);
                }
                location.href = r.url;
            }
        }
    });
}