// 상품평 보기
function recommView(idnum, totcnt, product_recomm_seq) {
	$(".answer").hide();
	$("#recomm_"+idnum).show();
}

//상품 만족도 탭보기
function viewReview() {
	$('#tabs').tabs("option","active",2);
}

function gotoTop() {
	$('body,html').stop().animate({
		scrollTop:0
	},600);
}

$(document).ready(function() {
	// main rolling banner
	if ($('#main_slides .slides_container div').size() > 1){
		$('#main_slides').slides({
			play:3000,
			generateNextPrev: true
		});
	} else {
		$('#main_slides .play, #main_slides .pause').hide();
	};


	// new product rolling banner
	if ($('#collection_slides .slides_container div').size() > 1){
		$('#collection_slides').slides({
			play:3000
		});
	} else {
		$('#collection_slides .play, #collection_slides .pause').hide();
	};

	// gnb
	$('.gnb>ul>li').bind("mouseenter focusin", function() {
		$('.gnb .gnbsub').removeClass('open');
		if ($(this).attr('class') == 'hassub') $('.gnbsub',this).addClass('open');
		$(this).find('img').eq(0).attr('src', $(this).find('img').attr('src').replace('x.gif','o.gif'));
	}).bind("mouseleave",function(){
		$('.gnb .gnbsub').removeClass('open');
		$(this).find('img').eq(0).attr('src', $(this).find('img').attr('src').replace('o.gif','x.gif'));
	}).bind("focusout",function(){
		$(this).find('img').eq(0).attr('src', $(this).find('img').attr('src').replace('o.gif','x.gif'));
	});


	// selectbox style
	var select = $("select#bcat");
	var select2 = $("select#bcat2");
	select.change(function(e){
		var select_name = $(this).children("option:selected").text();
		$(this).siblings("label").text(select_name);
	});
	select2.change(function(e){
		var select_name = $(this).children("option:selected").text();
		$(this).siblings("label").text(select_name);
	});

	// mpp
	$(".color_select").click(function(e) {
		e.preventDefault();
		var ctop = $(this).parents('li').offset().top;
		var cleft = $(this).parents('li').position().left + 43;
		if ($(this).parents('li').index() % 3 == 0) {
			cleft = cleft - 43;
		}
		$('#inline_colorchip').css({
			'top' : ctop,
			'left' : cleft
		});
		$('.cmask').css('height',$(document).height());
		$('.cmask, #inline_colorchip').show();
	});
	$('#inline_colorchip .close, .cmask').click(function(e) {
		e.preventDefault();
		$('.cmask, #inline_colorchip').hide();
		$('.prodList .btn').css('z-index','');
	});
	$('#inline_colorchip .colorList a').click(function(e) {
		e.preventDefault();
		$('#inline_colorchip .colorList a').removeClass('on');
		$(this).addClass('on');
		$('#inline_colorchip .pselect select option').eq($(this).index()).attr("selected", "selected");
		$('#inline_colorchip .smoosh img').attr('src' ,$(this).attr('isrc'));
	});
	$("#inline_colorchip .pselect select").change(function(){
		var index = $('option', this).index($('option:selected', this));
		$('#inline_colorchip .colorList a').removeClass('on');
		$('#inline_colorchip .colorList a').eq(index).addClass('on');
		$('#inline_colorchip .smoosh img').attr('src' ,$('#inline_colorchip .colorList a').eq(index).attr('isrc'));
	});

	//spp
	if ($("#tabs").size() > 0) {
		$("#tabs, #tabs2").tabs();
	}
	$('.thumbImg a').click(function(e) {
		e.preventDefault();
		$(this).parent().parent().find('.pimg>img').attr('src',$(this).find('img').attr('src'));
	});
	$('.colorchip .colorList a').bind("mouseenter focusin", function(e) {
		$('.colorchip .colorList a').removeClass('on');
		$(this).addClass('on');
		$('.colorchip .smoosh img').attr('src' ,$(this).find('span').attr('isrc'));
	});

	//estee edits
	$('.editsTabs .ctabs>li').click(function(e) {
		e.preventDefault();
		for (i=0; i<$(this).parent().find('li').size(); i++) {
			$(this).parent().find('img').eq(i).attr('src', $(this).parent().find('img').eq(i).attr('src').replace('o.','x.'));
		}
		$(this).find('img').attr('src', $(this).find('img').attr('src').replace('x.','o.'));
		$('.tabsdiv').hide();
		$($(this).find('a').attr('href')).show();
	});

	//brand story
	$('.brandstory .ctabs').each(function(n) {
		$('li', this).bind('click', function(e) {
			e.preventDefault();
			var ci = $(this).index();

			$('.brandstory .ctabs').each(function(l){
				$('img', this).each(function(m){
					if (m == ci) {
						$(this).attr('src', $(this).attr('src').replace('x.gif','o.gif'));
					} else {
						$(this).attr('src', $(this).attr('src').replace('o.gif','x.gif'));
					};
				});
			});
			$('.tabsdiv').hide();
			$('.tabsdiv').eq(ci).show();
			$('html, body').animate({scrollTop: $('.tabsdiv').eq(ci).offset().top}, 500);
		});
	});

	// style and tips main
	$('.styletips .tips a').each(function(n){
		var w = $(this).width(), h = $(this).height(),
			nh = h*1.1, nw = nh*w/h,
			mt = (nh-h)/2, ml = (nw-w)/2;
		$(this).data('w',w).data('h',h).data('nw',nw).data('nh',nh).data('mt',mt).data('ml',ml);
		$(this).bind('focusin mouseenter', function(e){
			var o = $(e.currentTarget);
			$('.bg img', o).stop().animate({
				marginTop:-o.data('mt'),
				marginLeft:-o.data('ml'),
				width:o.data('nw'),
				height:o.data('nh')
			}, 800);
		}).bind('focusout mouseleave', function(e){
			var o = $(e.currentTarget);
			$('.bg img', o).stop().animate({
				marginTop:0,
				marginLeft:0,
				width:o.data('w'),
				height:o.data('h')
			}, 800);
		}).bind('click', function(e){
			document.location.href = e.currentTarget.href;
		});
	});

	$('.styletips .relProd .plist li').each(function(n){
		var t = $('.txt',this);
		t.css({left:0});
		$(this).bind('focusin mouseenter', function(e) {
			t.stop().animate({
				bottom:0
			},500);
		}).bind('focusout mouseleave', function(e) {
			t.stop().animate({
				bottom:-t.outerHeight()
			},500);
		});
	});

	//tooltip
	if ($('.tooltip').size() > 0) {
		$('.tooltip').tooltipster({
			offsetX: 3,
			offsetY: -20,
			position: 'top-left'
		}).bind("mouseenter focusin", function() {
			$(this).tooltipster('show');
		}).bind("mouseleave focusout",function(){
			$(this).tooltipster('hide');
		});
	}
	if ($('.tooltip2').size() > 0) {
		$('.tooltip2').tooltipster({
			theme: 'tooltipster-noir'
		});
	}
});