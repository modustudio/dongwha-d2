$(function(){
	
	var mainProduct1 = function(){ 
		$("#mainProduct1").bxSlider({
			wrapperClass: 'main-slider1',
			minSlides: 4,
			maxSlides: 4,
			slideWidth: 253,
			infiniteLoop:false,
			pagerType: 'short',
			pagerSelector:'#mainProduct1Pager',
			prevSelector:'#mainProduct1Prev',
			nextSelector:'#mainProduct1Next'
		});
	};
	sliderFnc("#mainProduct1",mainProduct1);
	
	var mainPromotion = function(){ 
		$('#mainPromotion').bxSlider({
			wrapperClass: 'promotion-wrap',
			minSlides: 3,
			maxSlides: 3,
			slideWidth: 324,
			slideMargin: 22,
			infiniteLoop:false,
			pagerType: 'short',
			pagerSelector:'#promotionPager',
			prevSelector:'#promotionPrev',
			nextSelector:'#promotionNext'
		});
	};
	sliderFnc("#mainPromotion",mainPromotion);
	
	if ($('#mainOneDay>li').length > 2) {
		var mainOneDay = function(){ 
			var mainOneDayFnc = $('#mainOneDay').bxSlider({
				wrapperClass: 'main-oneday__list-wrap',
				minSlides: 2,
				maxSlides: 2,
				moveSlides: 2,
				slideWidth: 379,
				pagerType: 'short',
				pagerSelector:'#mainOneDayPager',
				prevSelector:'#mainOneDayPrev',
				nextSelector:'#mainOneDayNext',
				shrinkItems: true,
				infiniteLoop:false,
				onSliderLoad: init,
				onSlideAfter: counter,
				pager: false,
			});
			function init() {
				$('#mainOneDayPager').append('<div class="counter">1 / '+$('#mainOneDay').children('li:not(.bx-clone)').length+'</div>');
			}
			function counter() {
				$('#mainOneDayPager .counter').html((((mainOneDayFnc.getCurrentSlide() + 1) * 2) -1) + ' / ' + mainOneDayFnc.getSlideCount());
			}
		};
		sliderFnc("#mainOneDay",mainOneDay);
	}
	
	var mainOnlyOnline = function(){ 
		$('#mainOnlyOnline').bxSlider({
			wrapperClass: 'main-slider1',
			minSlides: 4,
			maxSlides: 4,
			slideWidth: 253,
			infiniteLoop:false,
			pagerType: 'short',
			pagerSelector:'#mainOnlyOnlinePager',
			prevSelector:'#mainOnlyOnlinePrev',
			nextSelector:'#mainOnlyOnlineNext'
		});
	};
	sliderFnc("#mainOnlyOnline",mainOnlyOnline);
	
	var mainBrands = function(){ 
		$('#mainBrands').bxSlider({
			wrapperClass: 'main-brands-wrap',
			minSlides: 4,
			maxSlides: 4,
			slideWidth: 253,
			infiniteLoop:false,
			pagerType: 'short',
			pagerSelector:'#mainBrandsPager',
			prevSelector:'#mainBrandsPrev',
			nextSelector:'#mainBrandsNext'
		});
	};
	sliderFnc("#mainBrands",mainBrands);
	
	var partnerBanner = function(){ 
		var partnerBannerFnc = $('#partnerBanner').bxSlider({
			wrapperClass: 'partner-banner-wrap',
			pagerSelector:'#partnerBannerPager',
			autoControlsSelector:'#partnerBannerPlay',
			controls: false,
			auto: true,
			autoControls: true,
			autoHover:true,
			hideControlOnEnd:true
		});
		$('#partnerBannerPager .bx-pager-item a').click(function(e){
			partnerBannerFnc.stopAuto();
			restart=setTimeout(function(){
				partnerBannerFnc.startAuto();
			},500);
		});
	};
	sliderFnc("#partnerBanner",partnerBanner);

});

$(function(){
	if ($(".main-visual:visible").length > 0) {
		
		$(".main-visual").append("<div class=\"main-visual__bg\"></div>");
		$(".main-visual__bg").css({
			"background-color":"#"+$("#mainVisualImg").find("a").eq(0).find("img").data("color"),
			"background-image":"url("+$("#mainVisualImg").find("a").eq(0).find("img").data("src")+")"
		})
		
		var cK = $("#mainVisualImg").find("a").eq(0).find("img[data-src]");
		cK.each(function(a) {
			$(this).attr("src", $(this).attr("data-src"))
		});
		var b8 = 0;
		var bb = setTimeout(function() {
			a9()
		}, 3000);
		$("#mainVisualCtrl").show().before("<ul id=\"mainVisualPager\" class=\"main-visual__pager\">");
		$("#mainVisualImg img").each(function(e){
			if ($(this).closest("a").siblings("ul").length > 0) {
				if (e == 0) { var d = "is-on";}
				$("<li><a href='"+$(this).closest("a").attr("href")+"'>"+$(this).attr("alt")+"</a><ul></ul></li>").addClass(d).appendTo("#mainVisualPager");
			} else {
				var b = ($(this).closest("ul").closest("li").index())+1;
				$("<li><a href='"+$(this).closest("a").attr("href")+"'>"+$(this).attr("alt")+"</a></li>").appendTo("#mainVisualPager>li:nth-of-type("+b+")>ul");
			}
			if (e == 0) { 
				$(this).parent().css("display","block");
			}
		});
		$("#mainVisualImg").on("mouseenter focusin focusout mouseleave", function(a) {
			a = a || window.event;
			if (a.type === "mouseenter" || a.type === "focusin") {
				clearTimeout(bb);
			} else {
				if ($("#mainVisualCtrl a.is-play").css("display") == "block") {
					return false;
				}
				bb = setTimeout(function() {
					a9()
				}, 3000)
			}
		});
		$("#mainVisualCtrl a").on("click", function(e) {
			e.preventDefault();
		 	if ($(this).hasClass("is-pause")) {
		 		$(this).hide().siblings(".is-play").css("display","block");
		 		clearTimeout(bb)
		 	} else {
		 		$(this).hide().siblings(".is-pause").css("display","block");
		 		bb = setTimeout(function() {
		 			a9()
		 		}, 3000)
		 	}
		});
		$("#mainVisualPager a").on("mouseenter focusin focusout mouseleave", function(d) {
			d = d || window.event;
			var a = $("#mainVisualPager a").index(this);
			var b = $("#mainVisualImg").find("a").eq(a).find("img[data-src]");
			if (d.type === "mouseenter" || d.type === "focusin") {
				clearTimeout(bb);
				b.each(function(e) {
					$(this).attr("src", $(this).attr("data-src"))
				});
				if ($(this).closest("li").find("ul").length > 0) {
					$(this).closest("li").addClass("is-on is-hover").siblings("li").removeClass("is-on");
					if (a < 0) {
						a = $("#mainVisualPager a").index($(this).siblings("ul").find("a").eq(0))
					}
					var b = $("#mainVisualImg").find("a").eq(a).find("img[data-src]");
					b.each(function(e) {
						$(this).attr("src", $(this).attr("data-src"))
					})
				}
				$("#mainVisualImg").find("a").hide().eq(a).css("display","block");
				var bgColor = "#"+$("#mainVisualImg").find("a").eq(a).find("img").data("color")
				var bgImage = "url("+$("#mainVisualImg").find("a").eq(a).find("img").data("src")+")"
				$(".main-visual__bg").css({
					"background-color": bgColor,
					"background-image": bgImage
				})
			} else {
				if (d.type === "focusout") {
					if ($(this).closest("li").find("ul").length == 0) {
						if ($(this).closest("li").index() == ($(this).closest("li").closest("ul").find("li").length)-1) {
							$(this).closest("li").closest("ul").closest("li").removeClass("is-hover");
						}
					}
				}
				if ($("#mainVisualCtrl a.is-play").css("display") == "block") {
					clearTimeout(bb);
				} else { 
					bb = setTimeout(function() {
						a9()
					}, 3000)
				}
			}
		});
		$("#mainVisualPager li").on("mouseleave",function(){
			$(this).removeClass("is-hover");
		});
	}

	function a9() {
		clearTimeout(bb);
		$("#mainVisualPager ul").closest("li").removeClass("is-hover");
		$("#mainVisualImg").find("a").hide();
		b8++;
		if (b8 === $("#mainVisualImg").find("a").length) {
			b8 = 0
		}
		var b = $("#mainVisualImg").find("a").eq(b8).find("img[data-src]");
		b.each(function(c) {
			$(this).attr("src", $(this).attr("data-src"))
		});
		$("#mainVisualImg").find("a").eq(b8).css("display","block");
		$(".main-visual__bg").css({
			"background-color": "#"+$("#mainVisualImg").find("a").eq(b8).find("img").data("color"),
			"background-image": "url("+$("#mainVisualImg").find("a").eq(b8).find("img").data("src")+")"
		})
		$("#mainVisualPager").find("a").parent().removeClass("is-on");
		$("#mainVisualPager").find("a").eq(b8).parents("li").addClass("is-on");
		cj()
	}
	function cj() {
		bb = setTimeout(function() {
			a9()
		}, 3000)
	}

})
