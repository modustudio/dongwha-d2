

function productDetailPager (currentSlide){
	
	var imgCnt = $(".product-detail_mainslide .slider4 .slide").not(".bx-clone").length;
	var createHtml = '';
	$(".product-detail_mainslide .slider4 .slide").not(".bx-clone").find("img").each(function(index) {
		if(index == currentSlide){
			createHtml += '<a data-slide-index="'+index+'" href="#n" class="active"><img src="'+$(this).attr('src')+'"/></a>';
		}else if (index != (imgCnt -1 )){
			createHtml += '<a data-slide-index="'+index+'" href="#n"><img src="'+$(this).attr('src')+'"/></a>';
		}else{
			createHtml += '<a data-slide-index="'+(index-1)+'" href="#n"><img src="'+$(this).attr('src')+'"/></a>';
		}
	});
	
	$(".product-detail_mainslide .bx-pager").append(createHtml);
	var bxpager = $('.bx-pager').bxSlider({
		slideWidth: 350,
		minSlides: 4,
		maxSlides: 1,
		moveSlides: 1,
		slideMargin: 0,
		autoControls: false,
		startSlide: 0,
		infiniteLoop: false,
		pager: false,
		controls: true,
		mode: 'vertical',
		wrapperClass: 'bx-wrapper2',
		touchEnabled: true,


	});
	if (imgCnt > 4) {
		$('.bx-controls.bx-has-controls-direction').css('display','block')
	}
	
}


$(document).ready(function(){
	if ($('.js-event-item').length > 0) {
		var eventItem = $('.js-event-item');
		var eventItemWrap = $('.js-event-item-wrap');
		var eventItemDesc = $('.js-event-item-desc');
		
		var eventItemHeightFix = function(){
			eventItem.filter(':even').each(function(){
				var itemEvenHeight = $(this).find(eventItemDesc).height();
				var itemOddHeight = $(this).next().find(eventItemDesc).height();
				// console.log( eventItem.filter(':even'))
				//console.log(itemEvenHeight+','+itemOddHeight)
				if (itemEvenHeight < itemOddHeight) {
					$(this).find(eventItemDesc).height(itemOddHeight);
				} else {
					$(this).next().find(eventItemDesc).height(itemEvenHeight);
				}
			})
		}
		var dd = function(){
			if (eventItemWrap.offset().top < $(window).scrollTop() + $(window).height() && eventItemWrap.offset().top > $(window).scrollTop() - eventItemWrap.height()) {
				eventItemHeightFix();
			}
		}
		$(window).on('scroll',dd);
		
	}
	$('.product-detail_event-more').on("click",function(e) {
		e.preventDefault();
		// $('.product-detail_event-moresub').stop(true,true).slideToggle(500,function(){$('.productDot').dotdotdot();eventItemHeightFix();});
		var target = $('.product-detail_event-moresub');
		//console.log(target.css("visibility"))
		if (target.css("visibility") == "hidden") {
			target.stop(true,true).animate({
				'max-height': '10000px'
			}, 500, function(){
				$('.productDot').dotdotdot();
				eventItemHeightFix();
			}).css({
				'visibility': 'visible'
			});
		} else {
			target.stop(true,true).animate({
				'max-height': '0'
			}, 500, function(){
				$(this).css({
					'visibility': 'hidden'
				})
			});
		}
		
	});
	if ($('.slider4>.slide').length > 1) {
		
		var productDetail = $('.slider4').bxSlider({
			slideWidth: 350,
			minSlides: 2,
			maxSlides: 2,
			moveSlides: 1,
			slideMargin: 0,
			controls: false,
			startSlide: 0,
			infiniteLoop: false,
			pagerCustom: '#bx-pager'
		});
		
		productDetailPager(productDetail.getCurrentSlide());
		
		
		
	}
	
	/*
	$('.product-detail_mainslide .bx-pager a').click(function(){
		if($(this).next().is('a')) {$(".product-detail_mainslide .bx-pager a:first-child").removeClass("cssClone");
		}else{$(".product-detail_mainslide .bx-pager a:first-child").addClass("cssClone");}
	});
	*/
});

// 탭 고정
$(function(){
	var t = $("#productTab");
	var p;
	if (t.length > 0) {
		$(window).on("scroll", function(e) {
			if ($(this).scrollTop() > (t.offset().top - (54+14))) {
				p = (t.offset().top - (54+14));
				t.addClass("js-fixed").before("<div class='js-tab-bg'></div>");
				$("#cardInfo").removeClass("is-on").hide();
			} else if ($(this).scrollTop() < p){
				t.removeClass("js-fixed").prev('.js-tab-bg').remove();
			}
		});
		t.find("a").each(function(){
			$(this).on("click",function(e){
				// if (t.hasClass("js-fixed")) $(window).scrollTop(p);
				if (t.hasClass("js-fixed")) {
					//console.log(t.offset().top);
					console.log(t.height());
					//$(window).scrollTop(p);
					$("body,html").animate({
			  			scrollTop: p - 1 
					}, 300,function(){
						console.log(p);
					});
				}
			})
		});
	}
});
$(function(){
	$(".table1").find("th").each(function(idx){
		keyword = new RegExp("성분"); 
		if (this.textContent.match(keyword)) {
			console.log(this.textContent)
		// if (this.textContent == "전성분" || this.textContent == "주요성분") {
			$(this).next().wrapInner("<div class='js-table1-wrap'><div class='js-table1-wrap-sort'></div></div>");
			var $wrap = $(this).next().find(".js-table1-wrap-sort")
			$wrap.clone().insertAfter($wrap).addClass("js-table1-wrap-full").removeClass("js-table1-wrap-sort").end().end().addClass("productDot");
			dot();
			
			if ($wrap.html() == $wrap.next().html()) {
				$wrap.next().remove();
				$wrap.unwrap().parent().text($wrap.text());
			} else { 
				$("<button type='button' class='js-table1-button-more'>더보기</button>").insertAfter($wrap.next(".js-table1-wrap-full"))
			}
		};
	})
	$(".js-table1-button-more").on("click",function(){
		$(this).toggleClass("is-open").siblings(".js-table1-wrap-sort").toggle().end().siblings(".js-table1-wrap-full").toggle();
	})
});

// 돋보기
$(function(){
	$(document).on({
		mouseenter: function () {
			var _this = $(this);
			_this.find(".magnifier-lens").css("background-image","url("+_this.find(".magnifier-img").data("ui-src")+")")
			$("#consoleLog").html(_this.find(".magnifier-lens").css("background-image"))
			if ($(".slide").length > 1) {
				$(".magnifier-preview").css({
					"visibility":"visible",
					"z-index":"1",
					"border" : "1px solid #ccc"
				})
				var margin = (1016 - (_this.width()*2))/2
				if ($(this).offset().left < parseInt($("body").width()/2)) {
					$(".magnifier-preview").css({
						"left": _this.width() + margin + "px"
					})
				} else {
					$(".magnifier-preview").css({
						"left": margin + "px"
					})
				}
			}
		},
		mouseleave: function () {
			$(".magnifier-preview").css({
				"visibility":"hidden",
				"z-index":"-1",
				"border": "none"
			})
			$(".magnifier-large").addClass("hidden");
			$(this).find(".magnifier-img").removeClass("opaque").next(".magnifier-lens").addClass("hidden");
			
		}
	},".slide");
})