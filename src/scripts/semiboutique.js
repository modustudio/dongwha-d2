$(function(){
	
	var boutiqueVisual = function(){ 
		var boutiqueVisualFnc = $('#boutiqueVisual').bxSlider({
			wrapperClass: 'partner-banner-wrap',
			pagerSelector:'#boutiqueVisualPager',
			autoControlsSelector:'#boutiqueVisualPlay',
			controls: false,
			auto: true,
			autoControls: true,
			autoHover:true,
			hideControlOnEnd:true
		});
		$('#boutiqueVisualPager .bx-pager-item a').click(function(e){
			boutiqueVisualFnc.stopAuto();
			restart=setTimeout(function(){
				boutiqueVisualFnc.startAuto();
			},500);
		});
	};
	sliderFnc("#boutiqueVisual",boutiqueVisual);
	
	var boutiquePromotion = function(){ 
		var boutiquePromotionFnc = $('#boutiquePromotion').bxSlider({
			wrapperClass: 'partner-banner-wrap',
			pagerSelector:'#boutiquePromotionPager',
			autoControlsSelector:'#boutiquePromotionPlay',
			controls: false,
			auto: true,
			autoControls: true,
			autoHover:true,
			hideControlOnEnd:true
		});
		$('#boutiquePromotionPager .bx-pager-item a').click(function(e){
			boutiquePromotionFnc.stopAuto();
			restart=setTimeout(function(){
				boutiquePromotionFnc.startAuto();
			},500);
		});
	};
	sliderFnc("#boutiquePromotion",boutiquePromotion);
	
})