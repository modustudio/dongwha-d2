



$(function(){
	$(".brand-tab3_head li a").on("click",function(e){
		var checkElement = $('.brand-tab3_foot .brand-tab_content-group');
		checkElement.hide();
	});

	$(".brand-tab3_body a").on("click",function(e){
		e.preventDefault();
		var checkElement = $('.brand-tab3_foot .brand-tab_content-group');
		var overlapBool = $(this).closest("li").hasClass("on");
		checkElement.show();
		if(overlapBool){
			checkElement.hide();
			$(this).closest("li").removeClass("on");
		}else{
			$(this).closest("li").siblings().removeClass("on").end().addClass("on");	
		}
	});
	$(document).on("click",function(e){
		var checkElement = $('.brand-tab3_foot .brand-tab_content-group');
		if ($(".brand-tab3_body li").hasClass("on") && ($(e.target).closest("li").hasClass("on") == false) && ($(e.target).closest(checkElement).length == 0)) {
			checkElement.hide();
		}
	});
	
})


