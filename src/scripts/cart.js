$(function(){		
	var partnerBanner = $('#partnerBanner').bxSlider({
		wrapperClass: 'partner-banner-wrap',
		pagerSelector:'#partnerBannerPager',
		autoControlsSelector:'#partnerBannerPlay',
		controls: false,
		auto: true,
		autoControls: true,
		autoHover:true,
		hideControlOnEnd:true
	});
	$('#partnerBannerPager .bx-pager-item a').click(function(e){
		partnerBanner.stopAuto();
		restart=setTimeout(function(){
			partnerBanner.startAuto();
		},500);
	});
});
