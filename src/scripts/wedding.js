
$(document).ready(function(){
	var weddingSlide1 = null;
	if ($('.wedding-slide1').length > 0) {
		var weddingSlide1 = $('.wedding-slide1').bxSlider({							
			slideWidth: 64,
			minSlides: 1,
			maxSlides: 5,
			moveSlides: 1,
			slideMargin: 2,
			startSlide: 0,
			pager: false,
			controls: false
		});
		var slide1Cnt = weddingSlide1.getSlideCount();
		if(slide1Cnt <= 5){
			$('.wedding-slide1-control__prev').remove();
			$('.wedding-slide1-control__next').remove();
			$('.wedding-slide1').parent().css({"left":"-17px"});
		}
	}
	$('.wedding-slide1-control__prev').click(function(){
		weddingSlide1.goToPrevSlide();
		return false;
	});
	$('.wedding-slide1-control__next').click(function(){
		weddingSlide1.goToNextSlide();
		return false;
	});
	
	var weddingSlide2 = null;
	if ($('.wedding-slide2').length > 0) {
		var weddingSlide2 = $('.wedding-slide2').bxSlider({							
			slideWidth: 64,
			minSlides: 1,
			maxSlides: 5,
			moveSlides: 1,
			slideMargin: 2,
			startSlide: 0,
			pager: false,
			controls: false
		});
		var slide2Cnt = weddingSlide2.getSlideCount();
		if(slide2Cnt <= 5){
			$('.wedding-slide2-control__prev').remove();
			$('.wedding-slide2-control__next').remove();
			$('.wedding-slide2').parent().css({"left":"-17px"});
		}
	}
	$('.wedding-slide2-control__prev').click(function(){
		weddingSlide2.goToPrevSlide();
		return false;
	});
	$('.wedding-slide2-control__next').click(function(){
		weddingSlide2.goToNextSlide();
		return false;
	});
	
	$('.wedding-event-slide__item').click(function(){
		var imgSrc = $(this).find('img').attr('src');
		$(this).parents('.wedding-event-slide').find('.wedding-event-slide__big-img').find('img').attr('src',imgSrc);
		return false;
	});
});



