//ie8 indexOf
$(function(){
	if (!Array.indexOf) {
		Array.prototype.indexOf = function (obj, start) {
			for (var i = (start || 0); i < this.length; i++) {
				if (this[i] == obj) {
					return i;
				}
			}
		};
	}
});

function isIE () {
	var myNav = navigator.userAgent.toLowerCase();
	return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}

$(function(){
	var list = $(".js-category-list");
	if (list.length > 0) {
		list.on("mouseenter", function(){
			$(this).addClass("is-over");
		});
		list.on("mouseleave", function(){
			$(this).removeClass("is-over");
		});
		
		list.find(".js-item1").on("mouseenter", function(){
			$(this).siblings().find(".is-over").removeClass("is-over").end().removeClass("is-over").end().addClass("is-over").find(".js-item2").eq(0).addClass("is-over");
		});
		list.find(".js-item2").on("mouseenter", function(){
			$(this).siblings().find(".is-over").removeClass("is-over").end().removeClass("is-over").end().addClass("is-over");
		});
		list.find(".js-item3").on("mouseenter", function(){
			$(this).siblings().removeClass("is-over").end().addClass("is-over");
		});
		list.find(".js-item1").children("a").on("focusin", function(){
			$(this).parent().siblings().find(".is-over").removeClass("is-over").end().removeClass("is-over").end().addClass("is-over").find(".js-item2").eq(0).addClass("is-over");
		});
		list.find(".js-item2").children("a").on("focusin", function(){
			$(this).parent().siblings().find(".is-over").removeClass("is-over").end().removeClass("is-over").end().addClass("is-over");
		});
		list.find(".js-item3").children("a").on("focusin", function(){
			$(this).parent().siblings().removeClass("is-over").end().addClass("is-over");
		});
		list.find("a").eq(-1).on("focusout", function(){
			list.removeClass("is-over");
		});
	}
});

// bxSlider
sliderFnc = function(el,fn){
	if ($(el).length > 0) {
		var state = 0;
		if ($(el).offset().top < $(window).scrollTop() + $(window).height() && $(el).offset().top > $(window).scrollTop() - $(el).height()) {
			fn();
		} else {
			var n = function() {
				if (state == 0 && $(el).offset().top < $(window).scrollTop() + $(window).height() && $(el).offset().top > $(window).scrollTop() - $(el).height()) {
					fn();
					state = 1;
				} 
			};
			$(window).bind("scroll", n);
		}
	}
};

//대분류
$(function(){
	if ($("#categoryMenu1").length > 0) {
		$("#categoryMenu1 a").each(function(){
			$(this).on("mouseenter focusin focusout", function(e){
				if (e.type === "mouseenter" || e.type === "focusin") {
					if ($(this).closest("li").find("ul").length > 0) {
						$(this).closest("li").addClass("is-hover");
					}
				} else if (e.type === "focusout") {
					if ($(this).closest("li").find("ul").length == 0){
						if ($(this).closest("li").index() == ($(this).closest("li").closest("ul").find("li").length)-1) {
							$(this).closest("li").closest("ul").closest("li").removeClass("is-hover");
						}
					}
				}
			});
		});
		$("#categoryMenu1>li").on("mouseleave", function(e){
			$(this).removeClass("is-hover");
		});
	}
});

//spinner
$(function(){
	$.fn.spinner = function() {
		this.each(function() {
			var el = $(this);
			if (el.closest(".spinner").length > 0) return;

			// add elements
			el.wrap('<div class="spinner"></div>');
			el.before('<span class="sub">&#45;</span>');
			el.after('<span class="add">&#43;</span>');

			// substract
			el.parent().on('click', '.sub', function () {
				if (el.val() > parseInt(el.attr('min')))
					el.val( function(i, oldval) { return --oldval; });
			});

			// increment
			el.parent().on('click', '.add', function () {
				if (el.val() < parseInt(el.attr('max')))
					el.val( function(i, oldval) { return ++oldval; });
			});
		});
	};
	$('.spinner-style').spinner();
});



//tab
var tab = function() {
	if ($("[data-ui-tab='true']").length > 0) {
		$("[data-ui-tab='true']").each(function() {
			var $tab = $(this);
			var hash = window.location.hash;
			var tabArray= [];
			$tab.find("a[href^=#]").each(function(idx){
				tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
			});
			for (var i in tabArray) {
				$(tabArray[i]).removeClass("is-show").addClass("is-hide");
			}
			if (hash && isNaN(tabArray.indexOf(hash)) === false) {
				sele($tab.find("a[href="+hash+"]"),"is-on","is-on");
				$(hash).removeClass("is-hide").addClass("is-show");
			} else {
				if ($tab.children().hasClass("is-on")) {
					$($tab.children(".is-on").find("a[href^=#]").attr("href")).removeClass("is-hide").addClass("is-show");
				} else {
					sele($tab.find("a[href^=#]").eq(0),"is-on","is-on");
					$(tabArray[0]).removeClass("is-hide").addClass("is-show");
				}
			}
		});
	}
	$(document).on("click","[data-ui-tab='true'] a[href^=#]",function(e){
		e.preventDefault();
		
		var $tab = $(this).closest("[data-ui-tab='true']");
		var hash = window.location.hash;
		var tabArray= [];
		$tab.find("a[href^=#]").each(function(idx){
			tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
		});
		
		if ($tab.data("ui-hash")) {
			if (!$(this).parent().hasClass("is-on")) window.location.hash = ($(this).attr("href"));
		}
		sele($(this),"is-on","is-on");
		for (var i in tabArray) {
			$(tabArray[i]).removeClass("is-show").addClass("is-hide");
		}
		$($(this).attr("href")).removeClass("is-hide").addClass("is-show");
		
	});
	function sele(el,show,hide) {
		$(el).parent().addClass(show).siblings().removeClass(hide);
	}
};
$(function() {
	tab();
});

//dotdotdot
var dot = function(){
	$('.productDot').each(function(){
		_$this = $(this);
		var h = (_$this.css("max-height") != "none") ? parseInt(_$this.css("max-height")) : null;
		_$this.dotdotdot({
			watch : true,
			height : h
		});
	});
};

$(function() {
	if ($(".productDot").length > 0) {
		dot();
	}
});

// layer popup background
function layerGrayOpen () {
	$(".layer_gray").fadeIn("fast");
}
function layerGrayClose () {
	$(".layer_gray").fadeOut("fast");
}
// layer popup open
function lpOpen (lp_id) {
	layerGrayOpen();
	var addAdminWidth = $("."+lp_id).width();
	var addAdminHeight = $("."+lp_id).height();
	$("."+lp_id).css({
		"margin-left":"-"+(addAdminWidth/2)+"px",
		"margin-top":"-"+(addAdminHeight/2)+"px",
		"left":"50%",
		"top":"50%"
	});
	$("."+lp_id).css("visibility","visible");
}
// layer popup close
function lpClose (lp_id) {
	layerGrayClose();
	$("."+lp_id).css("visibility","hidden");
	return false;
}

//Dialog
$(function(){
	var dialogState = 0;
	$(document).on("click","[data-ui-dialog]",function(e){
		tab();
		dot();
		console.log(dialogState);
		e.preventDefault();
		
		var dialog = "dialog";
		var $t = this.getAttribute('href') ? this.getAttribute('href') : $(this).data("ui-href");
		var m = $(this).data("ui-dialog");
		if (typeof m === "number") {
			$($t).find("."+dialog+"-layout").css("width",m+"px");
		} else if (m == "alert") {
			$($t).find("."+dialog+"-layout").css("width","400px");
		}
		
		if (m == "close") {
			$(this).parents("."+dialog).css("visibility","hidden");
			dialogState--;
			if (dialogState == 0) {
				$(".dimmed").hide().remove();
			}
		} else if ((typeof m === "number") || (m == "alert") ){
			if ((dialogState == 0) && ($(".dimmed").length <1)) $('<div class="dimmed"></div>').appendTo($(document.body)).end().show();
			$($t).css("visibility","visible");
			dialogState++;
		} else {
			alert("data-ui-dialog 에 정수값을 입력해주세요.");
		}
		
	});
});

// date picker
$(function() {
	var cnt = 0;
	
	$('.datepicker-input1').each(function(){
		var obj = 
		{
			autoHide: true,
			format: 'yyyy-mm-dd',
			daysMin: ['일', '월', '화', '수', '목', '금', '토'],
			yearFirst: true,
			yearSuffix: '년 ',
			monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
		}
		
		if($(this).attr("startDate") == 'today'){
			if(!isEmpty($(this).attr("calType"))){
				var calDate = "0";
				
				if(!isEmpty($(this).attr("calDate"))){
					calDate = $(this).attr("calDate");
				}
				
				obj["startDate"] = date.yyyymmddBar($(this).attr("calType"), calDate);
			}else{
			obj["startDate"] = date.yyyymmddBar();
		}
		
			console.log("calDate --> " + calDate);
			console.log("startDate --> " + obj["startDate"]);
		}
		
		$(this).datepicker(obj).on('show.datepicker', function() {
			cnt = 1;
		}).on('hide.datepicker', function() {
			cnt = 0;
		});
	});
		
	$('.datepicker-btn1').on('click', function(e) {
		var $t = $(this).prev();
		if (cnt == 1) {
			e.stopPropagation();
			$t.datepicker('hide');
		} else if (cnt == 0) {
			e.stopPropagation();
			$t.focus();
			$t.datepicker('show');
		}
	});
});

// Breadcrumb
$(function() {
	if ($(".crumb-menu").length > 0) {
		var $item = $(".breadcrumb__item.type-home~.breadcrumb__item");
		var $button = $item.find(".breadcrumb__link");

		$(".crumb-menu").each(function(idx){
			var _this = $(this);
			if ($(this).closest(".breadcrumb").hasClass != "breadcrumb_dropdown") {
				$(this).closest(".breadcrumb").addClass("breadcrumb_dropdown");
			}
			$button.eq(idx).on("click mouseenter focusin mouseleave",_this,function(e){ 
				e.preventDefault();
				if ( (e.type == "click" && (_this.hasClass("js-on") == false)) || e.type == "mouseenter" || e.type == "focusin" )  {
					_this.addClass("js-on");
				} else if ((e.type == "click" && (_this.hasClass("js-on") == true)) ) { 
					_this.removeClass("js-on");
				} else if ((e.type == "mouseleave") && ($(".crumb-menu:hover").length === 0))  { 
					_this.removeClass("js-on");
				}
			});
			_this.on("mouseenter mouseleave",function(e){
				if ((e.type == "mouseleave") && ($(".breadcrumb__link:hover").length === 0)) {
					_this.removeClass("js-on");
				} else {
					_this.addClass("js-on");
				}
			});
		});
	}
});

//ie8
/*
$(function(){
	function isIE () {
		var myNav = navigator.userAgent.toLowerCase();
		return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
	}
	if (isIE () && isIE() <= 8) {
		//checkbox
		if ($("input[type='checkbox'],input[type='radio']").length > 0) {
			$("input[type='checkbox'],input[type='radio']").each(function(){
				if ($(this).is(":checked") == true) $(this).addClass("is-checked");
			});
			$("input[type='checkbox'],input[type='radio']").on("click",function(){
				if ($(this).is(":checked") == true) {
					$(this).addClass("is-checked");
				} else {
					$(this).removeClass("is-checked");
				}
			});
		}
	}
});
*/

//placeholder
$(function(){
	var bj = navigator.userAgent.toLowerCase();
	if (bj.indexOf("msie 7") !== -1 || bj.indexOf("msie 8") !== -1 || bj.indexOf("msie 9") !== -1) {
		if ($("input[placeholder], textarea[placeholder]").length > 0) {
			$("input[placeholder] , textarea[placeholder]").each(function() {
				var k = $(this);
				var c = k.attr("placeholder");
				if (k.data("placeholderType") == "text") {
					var l = parseInt(k.css("fontSize"));
					var h = k.css("color");
					var a = parseInt(k.outerHeight(true));
					var e = parseInt(k.outerWidth(true));
					var g = parseInt(k.css("paddingLeft"));
					var q = parseInt(k.css("paddingRight"));
					var pt = parseInt(k.css("paddingTop"));
					var bl = parseInt(k.css("borderLeftWidth"));
					var br = parseInt(k.css("borderRightWidth"));
					var bt = parseInt(k.css("borderTopWidth"));
					var bb = parseInt(k.css("borderBottomWidth"));
					var lh = k.css("lineHeight");
					var f = parseInt((k.outerHeight() - l) / 2);
					var m = k.css("textAlign");
					if (parseInt(k.css("marginTop")) > 0) {
						f = f + parseInt(k.css("marginTop"));
					}
					if (parseInt(k.css("marginLeft")) > 0 && m == "left") {
						g = g + parseInt(k.css("marginLeft"));
					}
					if (parseInt(k.css("marginRight")) > 0 && m == "right") {
						g = g + parseInt(k.css("marginRight"));
					}
					//var d = "<div class='input-wrap' style='position:relative; display:inline-block; width:" + (e-g-q-bl-br) + "px; height:" + (a-bt-bb) + "px;' ></div>";
					var d = "<div class='input-wrap' style='position:relative; display:inline-block; width:" + (e) + "px; height:" + (a) + "px;' ></div>";
					//var b = "<span class='input-placeholder' style='position:absolute; margin:0; padding:0; top:"s + f + "px; " + (m == "right" ? "right" : "left") + ":" + g + "px;'>" + c + "</span>";
					
					var b = (this.nodeName == "TEXTAREA") ? "<span class='input-placeholder' style='overflow:hidden;position:absolute; top:0;left:0;right:0;bottom:0;padding-top:"+(bt+pt)+"px;padding-left:"+(g+bl)+"px;padding-right:"+q+"px;text-align:"+m+";font-size:"+l+"px;line-height:"+lh+"'>" + c + "</span>" : "<span class='input-placeholder' style='white-space:nowrap;overflow:hidden;position:absolute; top:0;left:0;right:0;bottom:0;padding-left:"+(g+bl)+"px;padding-right:"+q+"px;text-align:"+m+";line-height:" + a + "px;font-size:"+l+"px'>" + c + "</span>";
					
					k.wrap(d);
					k.parent("div").append(b);
					if (k.val()) {
						k.next().hide();
					}
					k.on("focusin focusout", function(n) {
						n = n || window.event;
						if (n.type == "focusin") {
							if ($.trim(k.val()) == "") {
								k.next().hide();
							}
						} else {
							if (n.type == "focusout") {
								if ($.trim(k.val()) == "") {
									k.next().show();
								}
							} else {
								k.addClass("focus");
							}
						}
					});
					k.next().on({
						click: function() {
							k.trigger("focusin").focus();
						}
					});
				} else {
					if (k.val() == "" && k.val() == undefined) {
						k.val(c);
					}
					k.on("focusin focusout", function(n) {
						n = n || window.event;
						if (n.type === "focusin") {
							if ($.trim(k.val()) === c) {
								k.val("").addClass("focus");
							}
						} else {
							if (n.type === "focusout") {
								if ($.trim(k.val()) === "") {
									k.removeClass("focus").val(c);
								}
							} else {
								k.addClass("focus");
							}
						}
					});
				}
			});
		}
	}
});

// dropDown
$(function(){
	var $dropDown = $("[data-ui-drop-down]");
	var $state = $dropDown.find(".js-state");
	var $list = $dropDown.find(".js-list");
	var $item = $dropDown.find(".js-item");
	var toggoleOpen = "is-open";
	var on = "is-on";
	
	$state.attr("tabindex","0");
	$state.on("click keypress",function(e){
		if ((e.keyCode == 13)||(e.type == 'click')) {
			$(this).siblings($list).toggleClass(toggoleOpen);
		}
	});
	$item.on("click",function(e){
		e.preventDefault();
		$(this).parent().addClass(on);
		$(this).parent().siblings().removeClass(on);
		$(this).closest($list).siblings($state).text($(this).text());
	});
	$dropDown.on("mouseleave",$list,function(){
		$(this).find($list).removeClass(toggoleOpen);
	});
	$(document).on("click",function(e){
		if (!$(e.target).hasClass("js-state") && $list.hasClass(toggoleOpen) && $(e.target).closest($list).hasClass(toggoleOpen) == false) {
			$list.removeClass(toggoleOpen);
		}
	});	

});


$(function(){
	$(".accordion-button").on("click",function(e){
		e.preventDefault();
		$('.accordion-content').stop(true,true).slideUp(200);
		$(".accordion-button").removeClass("on");
		$(this).addClass("on");


		var thisId = $(this).data('id');
		var checkElement = "";
		if(thisId == "accordion-table"){
			checkElement = $(this).parents('tr').next().find('.accordion-content');
		}else if(thisId == "accordion-ul"){
			checkElement = $(this).parent().next();
		}
		if(checkElement.is(':visible')) {
			$('.accordion-content').slideUp(200);
			$(this).removeClass("on");
			return false;
		}else if(!checkElement.is(':visible')) {
			$('.accordion-content').slideUp(200);
			checkElement.slideDown(200);
			return false;
		}
	});
});

$(function(){
	var tab = ".js-tab1-variable";
	if ($(tab).length > 0) {
		var itemLength = $(tab).find("li").length;
		if (itemLength > 6 && itemLength < 9) {
			$(tab).addClass("tab1_column-4");
		} else if (itemLength > 8) {
			$(tab).addClass("tab1_column-5");
		}
	}
});


$(function(){
	if ($(".tab8").length > 0) {
		$(".tab8").each(function(){
			$tab = $(".tab8");
			if ($(this).find("li").eq($(this).find("li").length-1).position().left > $(this).width()) {
				$(this).addClass("tab8-fixed");
			}
			$(this).find("li").css("visibility","visible");
		});
	}
});

$(function(){
	if ($(".tab12").length > 0) {
		$(".tab12").each(function(){
			$tab = $(".tab12");
			if ($(this).find("li").length < 6) {
				$(this).addClass("tab12_column-auto");
			}
		});
	}
});

$(function(){
	if ($(".upload-file").length > 0) {		
		var fileTarget = $('.upload-file');

		fileTarget.on('change', function() {
			
			var filename = (window.FileReader) ? $(this)[0].files[0].name : $(this).val().split('/').pop().split('\\').pop();
			$(this).parent().siblings('.upload-name').val(filename);
		});
		$(".upload-del").on("click",function(e) {
			e.preventDefault();
			$(this).siblings('.upload-name').val("");
			var bj = navigator.userAgent.toLowerCase();
			if (bj.indexOf("msie") !== -1) {
				$(this).siblings().find('.upload-file').replaceWith( $(this).siblings().find('.upload-file').clone(true) );
			} else {				
				$(this).siblings().find('.upload-file').val("");			
			}
		});
	}
});

$(function(){
	if ($(".pull-down").length > 0) {
		$('.pull-down').on("click",function(e){
			e.preventDefault();
			if ($(this).closest("tr").hasClass("is-on")) {
				$(this).closest("tr").next(".pull-down-row").find(".pull-down-content").animate({height:0},200,function() {
					$(this).closest("tr").prev().removeClass("is-on").end().remove();
				});
			} else {
				var path = ($(this).attr('href')).split("/");
				path = (path[path.length-1]).split(".");
				path = path.slice(0,-1).join('.');
				var n = $(this).closest("tr").children("td").length;
				var tr = $("<tr class=\"pull-down-row\"></tr>").insertAfter($(this).closest("tr").addClass("is-on"));
				var td = $("<td colspan=\""+n+"\"></td>").appendTo(tr);
				var template = Handlebars.templates[path];
				$("<div class=\"pull-down-content\"></div>").html(template()).prependTo(td).animate({"max-height":"2000px"},500);
				//$("<div class=\"pull-down-content\"></div>").load($(this).attr("href"),function(){
				//	$(this).animate({"max-height":"2000px"},500);
				//}).prependTo(td);
			}
		});
	}
});

$(function(){
	
	$.fn.singleNavSlide = function(r) {
		var a = {
			auto: true,
			pagingClass: "slide-pagination",
			imgAreaClass: "js-slide-list",
			activeClass: "is-on",
			thumbEvent: "mouseenter",
			speed: 2000
		};
		$.extend(a, r);
		var f = $(this);
		var e = f.find("." + a.imgAreaClass).find(">li");
		var p = e.size();
		var h = 0;
		var g = true;
		if (f.length < 1) {
			return;
		}
		var m = "";
		var t;
		if (f.offset().top < $(window).scrollTop() + $(window).height() && f.offset().top > $(window).scrollTop() - f.height()) {
			e.eq(0).find(".js-replace-img").each(function() {
				$(this).attr("src", $(this).attr("data-ui-src"));
			});
		} else {
			var n = function() {
				if (f.offset().top < $(window).scrollTop() + $(window).height() && f.offset().top > $(window).scrollTop() - f.height()) {
					if (a.auto == false) {
						e.eq(0).find(".js-replace-img").each(function() {
							$(this).attr("src", $(this).attr("data-ui-src"));
						});
					} else {
						e.find(".js-replace-img").each(function() {
							$(this).attr("src", $(this).attr("data-ui-src"));
						});
					}
					$(window).unbind("scroll", n);
				}
			};
			$(window).bind("scroll", n);
		}
		if (e.size() == 1) {
			f.find(".btn_area").hide();
			return;
		} else {
			f.find(".btn_area").show();
		}
		e.each(function(c) {
			if (c == 0) {
				$(this).addClass("is-on");
			}
			m += '<li' + (c == 0 ? " class='is-on'" : "") +'><a href="#n">' + ($(this).find('img').attr('alt')) + "</a></li>";
		});
		var o = '<ul class="' + a.pagingClass + '">' + m + "</ul>";
		f.append(o);
		var k = f.find("." + a.pagingClass).find("li");

		function s() {
			k.find("a").on("mouseenter , click", function(u) {
				if (u.type == a.thumbEvent) {
					l();
					h = $(this).parent().index();
					k.removeClass(a.activeClass);
					$(this).parent().addClass(a.activeClass);
					e.removeClass(a.activeClass);
					e.eq(h).addClass(a.activeClass);
					e.eq(h).find(".js-replace-img").each(function() {
						$(this).attr("src", $(this).attr("data-ui-src"));
					});
				}
				u.preventDefault();
			});
			f.on({
				"mouseenter , focusin": function() {
					if (g) {
						l();
					}
				},
				"mouseleave , focusout": function() {
					if (g) {
						b();
					}
				}
			});
			f.find(".js-pause").on({
				click: function(u) {
					$(this).hide();
					f.find(".js-play").show();
					g = false;
					l();
					u.preventDefault();
				}
			});
			f.find(".js-play").on({
				click: function(u) {
					$(this).hide();
					f.find(".js-pause").show();
					g = true;
					b();
					u.preventDefault();
				}
			});
		}

		function q() {
			var u = f.siblings("ul.btn_area.clear_both");
			if (u.length < 1) {
				return;
			}
			u.find("a").on({
				"mouseenter , focusin": function() {
					if (g) {
						l();
					}
				}
			});
			u.find(".js-prev").on("click", function(v) {
				h--;
				if (h < 0) {
					h = p - 1;
				}
				k.removeClass(a.activeClass);
				k.eq(h).addClass(a.activeClass);
				e.removeClass(a.activeClass);
				e.eq(h).addClass(a.activeClass);
				e.eq(h).find(".js-replace-img").each(function() {
					$(this).attr("src", $(this).attr("data-ui-src"));
				});
				v.preventDefault();
			});
			u.find(".js-next").on("click", function(v) {
				h++;
				if (h > p - 1) {
					h = 0;
				}
				k.removeClass(a.activeClass);
				k.eq(h).addClass(a.activeClass);
				e.removeClass(a.activeClass);
				e.eq(h).addClass(a.activeClass);
				e.eq(h).find(".js-replace-img").each(function() {
					$(this).attr("src", $(this).attr("data-ui-src"));
				});
				v.preventDefault();
			});
		}

		function b() {
			if (a.auto == true) {
				l();
				t = setInterval(function() {
					if (h < p - 1) {
						h++;
					} else {
						if (h == p - 1) {
							h = 0;
						}
					}
					k.removeClass(a.activeClass);
					k.eq(h).addClass(a.activeClass);
					e.removeClass(a.activeClass);
					e.eq(h).addClass(a.activeClass);
					if (f.offset().top < $(window).scrollTop() + $(window).height() && f.offset().top > $(window).scrollTop() - f.height()) {
						e.eq(h).find(".js-replace-img").each(function() {
							$(this).attr("src", $(this).attr("data-ui-src"));
						});
					}
				}, a.speed);
			}
		}

		function l() {
			clearInterval(t);
		}

		function d() {
			s();
			b();
			q();
		}
		d();
	};
	
	$("#categoryBanner").singleNavSlide({
		auto: true,
		pagingClass: "category-banner-pagination"
	});
	
	$("#brandBanner").singleNavSlide({
		auto: true,
		pagingClass: "brand-banner-pagination"
	});
	
});

$(function(){
	// if ($(".tab-list-content").length > 0 && $(".tab-list-content").hasClass("js-state") == false) {
	if ($(".tab-list-content").length > 0 ) {
		$(".tab-list-content").each(function(){
			var wrap = $(this);
			var item = wrap.find(".tab-list-content__item");
			if (isIE () && isIE() <= 8) {
				var regx = new RegExp(/tab-list-content_set-([0-9]*)/);
				var num = regx.exec(wrap.attr("class"))[1];
				for (var j = 0; j < item.length; j++) {
					wrap.find(item).eq(j).addClass("order-"+((j%num)+1));
				}
			}
			//wrap.addClass("js-state");
			// wrap.find("[tabindex=0]").on("mouseenter focusin",function(e){
			// 	$(this).closest(".tab-list-content__item").siblings().removeClass("is-on").end().addClass("is-on");
			// });
			// wrap.on("mouseleave",function(){
			// 	item.removeClass("is-on");
			// })
			wrap.find("[tabindex=0]").on("click keypress",function(e){
				if ((e.keyCode == 13)||(e.type == 'click')) {
					if ($(this).closest(item).hasClass("is-on")) {
						$(this).closest(item).removeClass("is-on");
					} else {
						$(this).closest(item).siblings().removeClass("is-on").end().addClass("is-on");
					}
				}
			});
			$(document).on("click",function(e){
				if (item.hasClass("is-on") && $(e.target).closest(item).hasClass("is-on") == false) {
					item.removeClass("is-on");
				}
			});
		});
	}
});

//상단검색
$(function(){
	var box = $("#searchResult");
	$(".header-search").find("input:text").on("click",function(){
		if (box.hasClass("js-over")) {
			box.html(null).removeClass("js-over");
		} else {
			var template = Handlebars.templates["_search_box"];
			box.html(template()).addClass("js-over");
			$(".search-resultbox__product-list:eq(0)").html(Handlebars.templates["_product"]);
		}
	});
	$(document).on("click",function(e){
		if (box.hasClass("js-over") && $(e.target).closest(".header-search").find(box).hasClass("js-over") == false) {
			box.html(null).removeClass("js-over");
		}
	});
});

// 윙배너 왼쪽
// $(function(){
// 	var wingLeft = $(".wing-left");
// 	var wingRight = $(".wing-right_menu");
// 	var headerHeight = $(".header").height();
// 	var margin = 14;
// 	var totalHeight = headerHeight+14
// 	
// 	wingLeft.offset({top:totalHeight});
// 	
// 	$(window).scroll(function(){
// 		if($(document).scrollTop() > totalHeight){
// 			wingLeft.css({"position":"fixed","top":margin+54+"px"});
// 			wingRight.css({"top":margin+"px"});
// 		}else{
// 			wingLeft.css({"position":"absolute","top":totalHeight+"px"});
// 			wingRight.css({"top":(totalHeight-($(window).scrollTop()))+"px"});
// 		}
// 	});
// })

function wingClose (thisObj) {
	$(thisObj).stop(false,false).animate({
		left:"0px"
	}, 200);
}
function wingOpen (thisObj, linkWidth){
	$(thisObj).stop(false,false).animate({
		left:"-"+linkWidth+"px"
	}, 200);
}
$(function(){
	$(".wing-title_close-button").click(function(){
		$(".wing-right ul li").removeClass('on');
		$(".wing-right").stop(false,false).animate({right:"-263px"}, 200);
	});
	$(".wing-right ul li a").mouseenter(function(){
		var linkWidth = $(this).find('span').outerWidth();
		if(!$(this).parent().hasClass("on")){
			wingOpen($(this).find('span'),linkWidth);
		}
	}).bind("mouseleave",function(){
		if(!$(this).parent().hasClass("on")){
			wingClose($(this).find('span'));
		}
	});
	$(".wing-right ul li a").on("click",function(e){
		var linkWidth = $(this).find('span').outerWidth();
		
		if($(this).parent().hasClass("wing-right01")){
			if(isLoginUser != "Y"){
				loginPopup();
				return;
			}else if(!$(this).parent().hasClass("on")){
				//Wing - 마이동화 쇼핑혜택 조회
				fn_wingMyInfo();
			}
		}else if($(this).parent().hasClass("wing-right03")){
			if(!$(this).parent().hasClass("on")){
				//Wing - 오늘본상품 조회
				refreshTodayProduct();
		}
		}else if($(this).parent().hasClass("wing-right04")){
			if(!$(this).parent().hasClass("on")){
				//Wing - 장바구니 조회
				refreshWingCart();
			}
		}
		
		if($(this).parent().hasClass("on")){
			wingClose($(".wing-link"));
			$(this).parent().removeClass('on');
			
			$(".wing-right").stop(false,false).animate({right:"-263px"}, 200);
		}else{
			var thisLink = $(this).data('link');
			if(typeof(thisLink) != "undefined"){
				$(".wing-right ul li").removeClass('on');
				$(this).parent().addClass("on");
				wingClose($(".wing-link"));
				
				$(".wing-body_wrap").css({"display":"none"});
				$("."+thisLink).css({"display":"block"});
				//$(".wing-right_content").empty().load(thisLink);
				$(".wing-right").stop(false,false).animate({right:"0px"}, 200);
			}
		}
	});
});


// 맨 위로
$(function(){
	$(".js-go-to-top").on('click', function(e) {
		e.preventDefault();
		var targetTop = ($($(this).attr("href")).offset()) ? ($($(this).attr("href")).offset().top) : 0;
		$('html, body').animate({
			scrollTop: 0 + targetTop
		}, 300);
	});
});

$(function(){
	var brandBest = function(){ 
		$('#brandBest').bxSlider({
			wrapperClass: 'brand-best-slider',
			minSlides: 4,
			maxSlides: 4,
			slideWidth: 253,
			infiniteLoop:false,
			pagerType: 'short',
			pagerSelector:'#brandBestPager',
			prevSelector:'#brandBestPrev',
			nextSelector:'#brandBestNext'
		});
	};
	sliderFnc("#brandBest",brandBest);

	var brandEvent = function(){ 
		$('#brandEvent').bxSlider({
			wrapperClass: 'brand-event-slider',
			minSlides: 3,
			maxSlides: 3,
			slideWidth: 324,
			slideMargin: 22,
			infiniteLoop:false,
			pagerType: 'short',
			pagerSelector:'#brandEventPager',
			prevSelector:'#brandEventPrev',
			nextSelector:'#brandEventNext'
		});
	};
	sliderFnc("#brandEvent",brandEvent);

});

// 숫자만 입력 가능케
$(function(){
	$('.only-number').keyup(function () { 
		this.value = this.value.replace(/[^0-9\.]/g,'');
	});
});


// 레인지 슬라이더
$(function () {
	if ($(".range-slider").length > 0) {
		$(".range-bar").each(function(){
			var $min = $(this).closest(".range-slider").find(".slider-margin-value-min");
			var $max =$(this).closest(".range-slider").find(".slider-margin-value-max");
			function numberWithCommas(x) {
				return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
			$(this).ionRangeSlider({
				hide_min_max: true,
				keyboard: true,
				type: 'double',
				//prefix: "$",
				//postfix: "%",
				grid: false,
				drag_interval: true,
				prettify_enabled: true,
				prettify_separator: ",",
				onStart: function(data) {
					$min.val((this.prefix)+(numberWithCommas(data.from))+(this.postfix));
					$max.val((this.prefix)+(numberWithCommas(data.to))+(this.postfix));
				},
				onChange: function (data) {
					//rangeSet();
					$min.val((this.prefix)+(numberWithCommas(data.from))+(this.postfix));
					$max.val((this.prefix)+(numberWithCommas(data.to))+(this.postfix));
				}
			});		
		});
		
	}
});

// 글자수 제한
$(function(){
	if ($(".js-check-length-input").length > 0 ) {
		$(".js-check-length-input").each(function(idx){
			$(this).on("keyup", function(){
				var limit = $(this).data("limit");
				var strLength = this.value.length;
				if(strLength > limit){
					this.value=this.value.substring(0,limit);
					this.focus();
					$(".js-check-length-output").eq(idx).html(limit);
				}else{
					$(".js-check-length-output").eq(idx).html(strLength); 
				}
			});
		});
	}
});

//lazy img 
$(function(){
	$(".js-lazy-img").each(function(){
		var $this = $(this);
		if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
			var src = $this.attr("data-ui-src");
			$this.attr("src", src);
		} else {
			var n = function() {
				if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
					var src = $this.attr("data-ui-src");
					$this.attr("src", src);
					$(window).unbind("scroll", n);
				}
			};
			$(window).bind("scroll", n);
			
		}
	});
});

//sale
$(function(){
	var saleSlider = function(){ 
		var saleSliderFnc = $('#saleSlider').bxSlider({
			wrapperClass: 'sale-slider',
			minSlides: 2,
			maxSlides: 2,
			slideWidth: 507.5,
			pagerSelector:'#saleSliderPager',
			autoControlsSelector:'#saleSliderAuto',
			controls: false,
			auto: true,
			autoControls: true,
			hideControlOnEnd:true,
			autoHover:true
		});

		$('#saleSliderPager .bx-pager-item a').click(function(e){
			saleSliderFnc.stopAuto();
			restart=setTimeout(function(){
				saleSliderFnc.startAuto();
			},500);
		});
	};
	sliderFnc("#saleSlider",saleSlider);
});

//produect over
$(function(){
	if ($(".item-over-top").length == 0 ) {
		$("<div class='item-over-top'></div><div class='item-over-left'></div><div class='item-over-right'></div><div class='item-over-bottom'></div>").appendTo("body");
	}
	$(document).on("mouseenter",".product-item",function(){
		$(this).addClass("is-over");
		var _top = $(this).offset().top;
		var _left = $(this).offset().left;
		var _width = this.offsetWidth;
		var _height = this.offsetHeight;
		$(".item-over-top").css({display:"block",top:_top+"px",left:_left+"px",width:_width+"px"});
		$(".item-over-bottom").css({display:"block",top:(_top+_height-1)+"px",left:_left+"px",width:_width+"px"});
		$(".item-over-left").css({display:"block",top:_top+"px",left:_left+"px",height:_height+"px"});
		$(".item-over-right").css({display:"block",top:_top+"px",left:(_left+_width)+"px",height:(_height)+"px"});
	}).on("mouseleave",".product-item",function(){
		$(this).removeClass("is-over");
		$(".item-over-top").css({display:"none"});
		$(".item-over-bottom").css({display:"none"});
		$(".item-over-left").css({display:"none"});
		$(".item-over-right").css({display:"none"});
	});
	$(document).on("mouseenter",".product-item_cn",function(){
		$(".item-over-top").addClass("type-cn");
		$(".item-over-bottom").addClass("type-cn");
		$(".item-over-left").addClass("type-cn");
		$(".item-over-right").addClass("type-cn");
	});
});


// 헤더 고정 + 윙 배너 고정 + 띠배너
$(function(){
	var header = ".header";
	var belt =  ".header__row";
	var beltTop = $(belt).offset().top;
	var wingLeft = ".wing-left";
	var wingRight = ".wing-right_menu:not(.cn)";
	var wingMargin = 14;
	var bandBtn = ".js-band-banner-btn";
	var bandBody = ".js-band-banner-body";
	var bandHeight = $(bandBody).height();

	var bandState = function(){
		if($(bandBody).hasClass("is-close")) {
			return 0;
		} else { 
			return bandHeight;
		}
	};

	if ($(bandBtn).length > 0) {
		$(bandBtn).on("click", function(e){
			if ($(bandBody).hasClass("is-close")) {
				$(bandBody).removeClass("is-close").animate({
					"margin-top": "0px"
				}, 100,function(){
					$(bandBtn).toggleClass("is-flip");
					beltTop = $(belt).offset().top;
				});
			} else {
				$(bandBody).addClass("is-close").animate({
					"margin-top": -(bandHeight)+"px"
				}, 100,function(){
					$(bandBtn).toggleClass("is-flip");
					beltTop = $(belt).offset().top;
				});
			}
			$(wingLeft).animate({"position":"absolute","margin-top":wingMargin+$(header).height()+bandState()+"px"},100);
			$(wingRight).animate({"margin-top":((wingMargin+$(header).height()+bandState())-($(window).scrollTop()))+"px"},100);
		});
	}
	$(wingLeft).css({"margin-top":(wingMargin+$(header).height()+bandState())});
	$(wingRight).css({"margin-top":((wingMargin+$(header).height()+bandState())-($(window).scrollTop()))+"px"});

	if ($(header).length > 0) {
		$(window).on("scroll", function(e) {
			if (($(this).scrollTop() > $(belt).offset().top) && (($(header).height() - $(belt).height()) < ($("body")[0].scrollHeight - $("body").height() - $(header).height()))) {
				$(header).addClass("js-fixed").css("top",($(header).height()-54)*-1);
				$(wingLeft).css({"position":"fixed","margin-top":wingMargin+$(belt).height()+"px"});
				$(wingRight).css({"margin-top":wingMargin+$(belt).height()+"px"});
			} else if ($(this).scrollTop() < beltTop){
				$(header).removeClass("js-fixed").css("top",0);
				$(wingLeft).css({"position":"absolute","margin-top":wingMargin+$(header).height()+bandState()+"px"});
				$(wingRight).css({"margin-top":((wingMargin+$(header).height()+bandState())-($(window).scrollTop()))+"px"});
			}
		});
	}
	
	var wingBanner = function(){ 
		var wingBannerFnc = $('.wing-banner-set').bxSlider({
			wrapperClass: 'wing-banner-set-wrap',
			default: 500,
			auto:true,
			controls:false
		});
		
		$('.wing-banner-set .bx-pager-item a').click(function(e){
			wingBannerFnc.stopAuto();
			restart=setTimeout(function(){
				wingBannerFnc.startAuto();
			},500);
		});
	};
	sliderFnc(".wing-banner-set",wingBanner);
	
	
});

//모바일버튼
$(function(){
	if (($(".js-mobile-button").length > 0) && ($(".js-mobile-body").length > 0)) {
		var button = ".js-mobile-button";
		var body = ".js-mobile-body";
		$(body).css({
			"margin-left": ((($(button).offset().left)-($("body").width()-1016)/2)-(($(body).width()-$(button).width())/2))+ "px"
		})
		$(button).on("click",function(e){
			e.preventDefault();
			if ($(this).hasClass("is-over")) {
				mobileClose();
			} else {
				mobileOpen();
			}
		});
		$(window).on("scroll", function(e) {
			mobileClose();
		});
		$(document).on("click",function(e){
			if ($(button).hasClass("is-over") && ($(e.target).closest(button).length == 0) && ($(e.target).closest(body).length == 0) ) {
				mobileClose();
			}
		});
	}
	function mobileClose() { 
		$(button).removeClass("is-over");
		$(body).fadeOut();
	}
	function mobileOpen() { 
		$(button).addClass("is-over");
		$(body).fadeIn();
	}
});



//하단 미니게시판
$(function(){
	if ($(".js-footer-board-tit").length > 0) {
		var btn = ".js-footer-board-tit";
		var body = ".js-footer-board-body";
		
		$(body).parent().css({
			"overflow":"hidden",
			"font-size":"0",
			"white-space":"nowrap"
		});
		$(body).css({
			"display": "inline-block",
			"width": "100%",
			"margin-right" : "30px",
			"vertical-align" : "top"
		});
		footTitle();
		$(btn).each(function(idx){

			$(this).on("click", function(e){
				e.preventDefault();
				$(btn).addClass("is-off").eq(idx).removeClass("is-off");
				//$(body).hide().eq(idx).show();
				$(body).eq(0).animate({
					"margin-left" : -(($(body).width() + parseInt($(body).css("margin-right"))) * idx) +"px"
				});
				footTitle();
			});
		});
		function footTitle() {
			$(body).prev().css("margin-left",function(){
				if ($(this).prev().prev()[0]) {
					return ($(this).prev().prev()[0].offsetLeft+$(this).prev().prev()[0].offsetWidth-$(body).eq(0).prev()[0].offsetLeft)+"px";
				} else {
					return "0px";
				}
			});
		}
	}
})

//패밀리사이트
$(function(){
	if ($(".js-family-btn").length > 0) {
		var btn = ".js-family-btn"
		var body = ".js-family-body"

		$(btn).attr("tabindex","0").on("click keypress",function(e){
			if ((e.keyCode == 13)||(e.type == 'click')) {
				$(btn).toggleClass("js-open")
				$(body).toggleClass("js-open")
			}
		})
		$(document).on("click",function(e){
			if ($(btn).hasClass("js-open") && ($(e.target).closest(btn).length == 0) && ($(e.target).closest(body).length == 0) ) {
				$(btn).toggleClass("js-open")
				$(body).toggleClass("js-open")
			}
		});
	}
})

//공통 토글
$(function(){
	if ($("[data-ui-toggle]").length > 0) {
		$("[data-ui-toggle]").on("click", function(e){
			e.preventDefault();
			var target = $(this).data("ui-toggle");
			
			if ($(target).hasClass("is-on")) {
				$(target).hide().removeClass("is-on");
			} else {
				$(target).show().addClass("is-on");
				if ($(window).scrollTop() < ($(target).offset().top - (($("body").height()-$(target).height())/2))) {
					$('html, body').animate({
						scrollTop: 0 + $(target).offset().top - (($("body").height()-$(target).height())/2)
					}, 500);
				}
			}
		})
	}
})



$(function(){
	if ($(".js-dwday-wrap1").length > 0) {
		$("<div class='dwday-wrap1-slider-pager'></div>").insertBefore(".js-dwday-wrap1")
		var dwdayWrapSlider = function(){ 
			var dwdayWrapFnc = $(".js-dwday-wrap1").bxSlider({
				wrapperClass: 'dwday-wrap1-slider',
				controls: false,
				auto: false,
				minSlides: 1,
				maxSlides: 1,
				//autoControls: true,
				hideControlOnEnd:true,
				autoHover:true,
				infiniteLoop:false,
				onSliderLoad : function(){
					//$("<div class='dwday-wrap1-slider-pager'></div>").appendTo(".js-dwday-wrap1")
				},
				pagerType: 'short',
				pagerSelector:'.dwday-wrap1-slider-pager'
			});
			
			$("<button type='button' class='dwday-wrap1-slider-button dwday-wrap1-slider-button_prev'>이전</button>").appendTo(".dwday-wrap1-slider").on("click",function(){
				dwdayWrapFnc.goToPrevSlide();
			})
			$("<button type='button' class='dwday-wrap1-slider-button dwday-wrap1-slider-button_next'>다음</button>").appendTo(".dwday-wrap1-slider").on("click",function(){
				dwdayWrapFnc.goToNextSlide();
			})
		
		};
		sliderFnc(".js-dwday-wrap1",dwdayWrapSlider);
	}
	if ($(".dwday-tab").find("li").length > 6) {
		$(".dwday-tab").addClass("dwday-tab_type-slider");
		var dwdayTabSlider = function(){ 
			var dwdayTabFnc = $('#dwdayTab').bxSlider({
				wrapperClass: 'dwday-tab-slider',
				minSlides: 6,
				maxSlides: 6,
				slideWidth: 158,
				pager:false,
				controls: false,
				auto: false,
				//autoControls: true,
				hideControlOnEnd:true,
				autoHover:true,
				infiniteLoop:false
			});
			$("<button type='button' class='dwday-tab-slider-button dwday-tab-slider-button_prev'>이전</button>").appendTo(".dwday-tab").on("click",function(){
				dwdayTabFnc.goToPrevSlide();
			})
			$("<button type='button' class='dwday-tab-slider-button dwday-tab-slider-button_next'>다음</button>").appendTo(".dwday-tab").on("click",function(){
				dwdayTabFnc.goToNextSlide();
			})
		
		};
		sliderFnc("#dwdayTab",dwdayTabSlider);	
	}
	$(".js-dwday-brand-best").each(function(){
		var _this = $(this)
		var dwdayBrandBestSlider = function(){ 
			var dwdayBrandBestFnc = _this.bxSlider({
				wrapperClass: 'dwday-brand-best-wrap',
				minSlides: 3,
				maxSlides: 3,
				slideWidth: 338,
				pager:false,
				controls: false,
				auto: false,
				//autoControls: true,
				hideControlOnEnd:true,
				autoHover:true,
				infiniteLoop:false,
				moveSlides: 1
			});
			$("<button type='button' class='dwday-brand-best-slider-button dwday-brand-best-slider-button_next'>다음</button>").appendTo(_this.closest(".dwday-brand-best-wrap")).on("click",function(){
				dwdayBrandBestFnc.goToNextSlide();
			})
			$("<button type='button' class='dwday-brand-best-slider-button dwday-brand-best-slider-button_prev'>이전</button>").appendTo(_this.closest(".dwday-brand-best-wrap")).on("click",function(){
				dwdayBrandBestFnc.goToPrevSlide();
			})
		};

		sliderFnc(".js-dwday-brand-best",dwdayBrandBestSlider);	
	})

})