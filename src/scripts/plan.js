
$(function(){
	$(".js-toggle-body").hide();
	$(".js-toggle-btn").click(function(evt){
		$(".js-toggle-body").toggle();
		evt.preventDefault();
	});


	$('.plan-brandmore').on("click",function(evt) {
		evt.preventDefault();
		var brandListHeight = $(".timesale-brand_ci2 ul").height();
		if($(this).hasClass("on")){
			$(this).removeClass("on");
			//$('.timesale-brand_ci2').animate({
			//	'maxHeight': 128
			//}, 200);
			$('.timesale-brand_ci2').css({"max-height":"128px"});
		}else {
			$(this).addClass("on");
			//$('.timesale-brand_ci2').animate({
			//	'maxHeight': 1000
			//}, 200);
			$('.timesale-brand_ci2').css({"max-height":"none"});
		}
	});
});

function promo_rolling() {
	var option = {
		bool: false,
		id: null,
		sec: 3000
	};
	var state = false;
	rollingEvent();
	rollingInterval(true);

	function rollingEvent() {
		var wrap = $(".banner-list1");
		var list = wrap.find(".banner-list1__list");
		var prevBtn = wrap.find(".banner-list1__btn-prev");
		var nextBtn = wrap.find(".banner-list1__btn-next");
		var listItemLength = list.find(">li").length;
		if (listItemLength <= 8) {
			$(prevBtn).addClass("end");
			$(nextBtn).addClass("end")
		} else {
			$(prevBtn).removeClass("end");
			$(nextBtn).removeClass("end")
		}
		$(list).find(">li").eq(0).addClass("is-on").find("img.js-lazy-img").each(function() {
			var src = $(this).attr("data-ui-src");
			$(this).attr("src", src)
		});
		$(list).find(">li").each(function(idx) {
			$(this).on({
				mouseenter: function() {
					$(list).find(">li").removeClass("is-on");
					$(this).addClass("is-on");
					$(this).find("img.js-lazy-img").each(function() {
						var src = $(this).attr("data-ui-src");
						$(this).attr("src", src)
					});
					if (listItemLength === 8) {
						$(prevBtn).addClass("end");
						$(nextBtn).addClass("end")
					} else {
						$(prevBtn).removeClass("end");
						$(nextBtn).removeClass("end")
					}
					state = true
				},
				mouseleave: function() {
					state = false;
					if (option.bool) {
						rollingInterval(true)
					}
				}
			})
		});
		$(prevBtn).on({
			click: function(evt) {
				var itemOn = $(list).find(">li.is-on").index();
				$(list).find(">li").removeClass("is-on");
				if (itemOn < 1 && listItemLength >= 8) {
					var itemFirst = $(list).find(">li:first");
					var itemLast = $(list).find(">li:last");
					$(itemLast).insertBefore($(itemFirst));
					$(list).find(">li:eq(0)").addClass("is-on");
					$(list).find(">li:eq(0)").find("img.js-lazy-img").each(function() {
						var src = $(this).attr("data-ui-src");
						$(this).attr("src", src)
					})
				} else {
					itemOn = (itemOn == 0) ? listItemLength : itemOn;
					$(list).find(">li:eq(" + (itemOn - 1) + ")").addClass("is-on");
					$(list).find(">li:eq(" + (itemOn - 1) + ")").find("img.js-lazy-img").each(function() {
						var src = $(this).attr("data-ui-src");
						$(this).attr("src", src)
					})
				}
				evt.preventDefault()
			},
			mouseenter: function() {
				state = true
			},
			mouseleave: function() {
				state = false;
				if (option.bool) {
					rollingInterval(true)
				}
			}
		});
		/*
		$(nextBtn).on({
			click: function(evt) {
				var itemOn = $(list).find(">li.is-on").index();
				$(list).find(">li").removeClass("is-on");
				if (itemOn > 6) {
					var itemFirst = $(list).find(">li:first");
					var itemLast = $(list).find(">li:last");
					$(itemFirst).insertAfter($(itemLast));
					$(list).find(">li:eq(7)").addClass("is-on");
					$(list).find(">li:eq(7)").find("img.js-lazy-img").each(function() {
						var src = $(this).attr("data-ui-src");
						$(this).attr("src", src)
					})
				} else {
					itemOn = (itemOn < 0) ? itemOn - 1 : itemOn;
					itemOn = (itemOn + 1 >= listItemLength) ? -1 : itemOn;
					$(list).find(">li:eq(" + (itemOn + 1) + ")").addClass("is-on");
					$(list).find(">li:eq(" + (itemOn + 1) + ")").find("img.js-lazy-img").each(function() {
						var src = $(this).attr("data-ui-src");
						$(this).attr("src", src)
					})
				}
				evt.preventDefault()
			},
			mouseenter: function() {
				state = true
			},
			mouseleave: function() {
				state = false;
				if (option.bool) {
					rollingInterval(true)
				}
			}
		});
		*/
		$(nextBtn).on("click keypress mouseenter mouseleave",function(evt){
			if (evt.type == 'click' || evt.type == 'keypress') {
				var itemOn = $(list).find(">li.is-on").index();
				$(list).find(">li").removeClass("is-on");
				if (itemOn > 6) {
					var itemFirst = $(list).find(">li:first");
					var itemLast = $(list).find(">li:last");
					$(itemFirst).insertAfter($(itemLast));
					$(list).find(">li:eq(7)").addClass("is-on");
					$(list).find(">li:eq(7)").find("img.js-lazy-img").each(function() {
						var src = $(this).attr("data-ui-src");
						$(this).attr("src", src)
					})
				} else {
					itemOn = (itemOn < 0) ? itemOn - 1 : itemOn;
					itemOn = (itemOn + 1 >= listItemLength) ? -1 : itemOn;
					$(list).find(">li:eq(" + (itemOn + 1) + ")").addClass("is-on");
					$(list).find(">li:eq(" + (itemOn + 1) + ")").find("img.js-lazy-img").each(function() {
						var src = $(this).attr("data-ui-src");
						$(this).attr("src", src)
					})
				}
				evt.preventDefault();
			} else if (evt.type == 'mouseenter') {
				state = true
			} else if (evt.type == 'mouseleave') {
				state = false;
				if (option.bool) {
					rollingInterval(true)
				}
			}
		});
		
		if ($(wrap).find(".banner-list1__btn-play").length > 0 ) {			
			$(wrap).find(".banner-list1__btn-pause").on({
				click: function(evt) {
					$(this).hide();
					$(wrap).find(".banner-list1__btn-play").show();
					option.bool = false;
					rollingInterval(false)
					evt.preventDefault()
				}
			});
			$(wrap).find(".banner-list1__btn-play").on({
				click: function(evt) {
					$(this).hide();
					$(wrap).find(".banner-list1__btn-pause").show();
					option.bool = true;
					rollingInterval(true)
					evt.preventDefault()
				}
			});
		}
	}

	function rollingInterval(elem) {
		var itemLength = $(".banner-list1__list").find(">li").length;
		clearInterval(option.id);
		if (itemLength <= 1) {
			return false
		}
		if (elem) {
			option.id = setInterval(function() {
				if (!state) {
					$(".banner-list1__btn-next").trigger("keypress")
				} else {
					rollingInterval(true)
				}
			}, option.sec)
		}
	}
}

$(function(){
	if ($(".banner-list1:visible").length > 0) {
		promo_rolling()
	}
})
	
