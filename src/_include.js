var include = $('.include');
include.each(function(idx) {
	var path = ($(this).data('file')).split("/");
	path = (path[path.length-1]).split(".");
	path = path.slice(0,-1).join('.');
	var template = Handlebars.templates[path];
	$(this).html(template());
	$(this).children().first().unwrap();
});